FROM adoptopenjdk/openjdk11
MAINTAINER chris@casualcode.de
COPY build/libs/*.jar /home/app.jar
CMD ["java","-jar","/home/app.jar"]
