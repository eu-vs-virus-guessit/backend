export const enum SessionState {
  LOBBY = 'LOBBY',
  GAME_IN_PROGRESS = 'GAME_IN_PROGRESS',
  GAME_ENDED = 'GAME_ENDED'
}
