export const enum GameState {
  THINK_PHASE = 'THINK_PHASE',
  VOTE_PHASE = 'VOTE_PHASE',
  GUESSING_PHASE = 'GUESSING_PHASE'
}
