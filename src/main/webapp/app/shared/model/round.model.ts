import { Moment } from 'moment';
import { IGuess } from 'app/shared/model/guess.model';

export interface IRound {
  id?: number;
  iteration?: number;
  dateCreated?: Moment;
  dateModified?: Moment;
  word?: string;
  guesses?: IGuess[];
  gameId?: number;
}

export class Round implements IRound {
  constructor(
    public id?: number,
    public iteration?: number,
    public dateCreated?: Moment,
    public dateModified?: Moment,
    public word?: string,
    public guesses?: IGuess[],
    public gameId?: number
  ) {}
}
