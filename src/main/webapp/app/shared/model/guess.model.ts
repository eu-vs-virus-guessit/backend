import { Moment } from 'moment';
import { GuessType } from 'app/shared/model/enumerations/guess-type.model';

export interface IGuess {
  id?: number;
  content?: string;
  score?: number;
  type?: GuessType;
  dateCreated?: Moment;
  dateModified?: Moment;
  roundId?: number;
  playerId?: number;
}

export class Guess implements IGuess {
  constructor(
    public id?: number,
    public content?: string,
    public score?: number,
    public type?: GuessType,
    public dateCreated?: Moment,
    public dateModified?: Moment,
    public roundId?: number,
    public playerId?: number
  ) {}
}
