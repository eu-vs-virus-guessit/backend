import { Moment } from 'moment';
import { IGuess } from 'app/shared/model/guess.model';

export interface IPlayer {
  id?: number;
  name?: string;
  admin?: boolean;
  dateCreated?: Moment;
  dateModified?: Moment;
  guesses?: IGuess[];
  sessionId?: number;
}

export class Player implements IPlayer {
  constructor(
    public id?: number,
    public name?: string,
    public admin?: boolean,
    public dateCreated?: Moment,
    public dateModified?: Moment,
    public guesses?: IGuess[],
    public sessionId?: number
  ) {
    this.admin = this.admin || false;
  }
}
