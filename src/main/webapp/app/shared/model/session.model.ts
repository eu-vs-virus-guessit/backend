import { Moment } from 'moment';
import { IPlayer } from 'app/shared/model/player.model';
import { SessionState } from 'app/shared/model/enumerations/session-state.model';

export interface ISession {
  id?: number;
  code?: string;
  name?: string;
  state?: SessionState;
  dateCreated?: Moment;
  dateModified?: Moment;
  langKey?: string;
  maxPlayers?: number;
  gameId?: number;
  players?: IPlayer[];
}

export class Session implements ISession {
  constructor(
    public id?: number,
    public code?: string,
    public name?: string,
    public state?: SessionState,
    public dateCreated?: Moment,
    public dateModified?: Moment,
    public langKey?: string,
    public maxPlayers?: number,
    public gameId?: number,
    public players?: IPlayer[]
  ) {}
}
