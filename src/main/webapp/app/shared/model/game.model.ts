import { Moment } from 'moment';
import { IRound } from 'app/shared/model/round.model';
import { GameState } from 'app/shared/model/enumerations/game-state.model';

export interface IGame {
  id?: number;
  maxPlayers?: number;
  state?: GameState;
  numberOfRounds?: number;
  dateCreated?: Moment;
  dateModified?: Moment;
  rounds?: IRound[];
  sessionId?: number;
}

export class Game implements IGame {
  constructor(
    public id?: number,
    public maxPlayers?: number,
    public state?: GameState,
    public numberOfRounds?: number,
    public dateCreated?: Moment,
    public dateModified?: Moment,
    public rounds?: IRound[],
    public sessionId?: number
  ) {}
}
