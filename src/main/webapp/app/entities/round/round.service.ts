import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRound } from 'app/shared/model/round.model';

type EntityResponseType = HttpResponse<IRound>;
type EntityArrayResponseType = HttpResponse<IRound[]>;

@Injectable({ providedIn: 'root' })
export class RoundService {
  public resourceUrl = SERVER_API_URL + 'api/rounds';

  constructor(protected http: HttpClient) {}

  create(round: IRound): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(round);
    return this.http
      .post<IRound>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(round: IRound): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(round);
    return this.http
      .put<IRound>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRound>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRound[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(round: IRound): IRound {
    const copy: IRound = Object.assign({}, round, {
      dateCreated: round.dateCreated && round.dateCreated.isValid() ? round.dateCreated.toJSON() : undefined,
      dateModified: round.dateModified && round.dateModified.isValid() ? round.dateModified.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated ? moment(res.body.dateCreated) : undefined;
      res.body.dateModified = res.body.dateModified ? moment(res.body.dateModified) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((round: IRound) => {
        round.dateCreated = round.dateCreated ? moment(round.dateCreated) : undefined;
        round.dateModified = round.dateModified ? moment(round.dateModified) : undefined;
      });
    }
    return res;
  }
}
