import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRound, Round } from 'app/shared/model/round.model';
import { RoundService } from './round.service';
import { RoundComponent } from './round.component';
import { RoundDetailComponent } from './round-detail.component';
import { RoundUpdateComponent } from './round-update.component';

@Injectable({ providedIn: 'root' })
export class RoundResolve implements Resolve<IRound> {
  constructor(private service: RoundService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRound> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((round: HttpResponse<Round>) => {
          if (round.body) {
            return of(round.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Round());
  }
}

export const roundRoute: Routes = [
  {
    path: '',
    component: RoundComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'Rounds'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RoundDetailComponent,
    resolve: {
      round: RoundResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Rounds'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RoundUpdateComponent,
    resolve: {
      round: RoundResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Rounds'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RoundUpdateComponent,
    resolve: {
      round: RoundResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Rounds'
    },
    canActivate: [UserRouteAccessService]
  }
];
