import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRound } from 'app/shared/model/round.model';
import { RoundService } from './round.service';

@Component({
  templateUrl: './round-delete-dialog.component.html'
})
export class RoundDeleteDialogComponent {
  round?: IRound;

  constructor(protected roundService: RoundService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.roundService.delete(id).subscribe(() => {
      this.eventManager.broadcast('roundListModification');
      this.activeModal.close();
    });
  }
}
