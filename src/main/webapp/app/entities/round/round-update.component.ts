import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IRound, Round } from 'app/shared/model/round.model';
import { RoundService } from './round.service';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game/game.service';

@Component({
  selector: 'jhi-round-update',
  templateUrl: './round-update.component.html'
})
export class RoundUpdateComponent implements OnInit {
  isSaving = false;
  games: IGame[] = [];

  editForm = this.fb.group({
    id: [],
    iteration: [null, [Validators.required]],
    dateCreated: [null, [Validators.required]],
    dateModified: [null, [Validators.required]],
    word: [null, [Validators.required]],
    gameId: []
  });

  constructor(
    protected roundService: RoundService,
    protected gameService: GameService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ round }) => {
      if (!round.id) {
        const today = moment().startOf('day');
        round.dateCreated = today;
        round.dateModified = today;
      }

      this.updateForm(round);

      this.gameService.query().subscribe((res: HttpResponse<IGame[]>) => (this.games = res.body || []));
    });
  }

  updateForm(round: IRound): void {
    this.editForm.patchValue({
      id: round.id,
      iteration: round.iteration,
      dateCreated: round.dateCreated ? round.dateCreated.format(DATE_TIME_FORMAT) : null,
      dateModified: round.dateModified ? round.dateModified.format(DATE_TIME_FORMAT) : null,
      word: round.word,
      gameId: round.gameId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const round = this.createFromForm();
    if (round.id !== undefined) {
      this.subscribeToSaveResponse(this.roundService.update(round));
    } else {
      this.subscribeToSaveResponse(this.roundService.create(round));
    }
  }

  private createFromForm(): IRound {
    return {
      ...new Round(),
      id: this.editForm.get(['id'])!.value,
      iteration: this.editForm.get(['iteration'])!.value,
      dateCreated: this.editForm.get(['dateCreated'])!.value
        ? moment(this.editForm.get(['dateCreated'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModified: this.editForm.get(['dateModified'])!.value
        ? moment(this.editForm.get(['dateModified'])!.value, DATE_TIME_FORMAT)
        : undefined,
      word: this.editForm.get(['word'])!.value,
      gameId: this.editForm.get(['gameId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRound>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IGame): any {
    return item.id;
  }
}
