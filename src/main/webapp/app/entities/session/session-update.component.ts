import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISession, Session } from 'app/shared/model/session.model';
import { SessionService } from './session.service';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game/game.service';

@Component({
  selector: 'jhi-session-update',
  templateUrl: './session-update.component.html'
})
export class SessionUpdateComponent implements OnInit {
  isSaving = false;
  games: IGame[] = [];

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required]],
    name: [null, [Validators.required]],
    state: [null, [Validators.required]],
    dateCreated: [null, [Validators.required]],
    dateModified: [null, [Validators.required]],
    langKey: [],
    maxPlayers: [],
    gameId: []
  });

  constructor(
    protected sessionService: SessionService,
    protected gameService: GameService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ session }) => {
      if (!session.id) {
        const today = moment().startOf('day');
        session.dateCreated = today;
        session.dateModified = today;
      }

      this.updateForm(session);

      this.gameService
        .query({ filter: 'session-is-null' })
        .pipe(
          map((res: HttpResponse<IGame[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IGame[]) => {
          if (!session.gameId) {
            this.games = resBody;
          } else {
            this.gameService
              .find(session.gameId)
              .pipe(
                map((subRes: HttpResponse<IGame>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IGame[]) => (this.games = concatRes));
          }
        });
    });
  }

  updateForm(session: ISession): void {
    this.editForm.patchValue({
      id: session.id,
      code: session.code,
      name: session.name,
      state: session.state,
      dateCreated: session.dateCreated ? session.dateCreated.format(DATE_TIME_FORMAT) : null,
      dateModified: session.dateModified ? session.dateModified.format(DATE_TIME_FORMAT) : null,
      langKey: session.langKey,
      maxPlayers: session.maxPlayers,
      gameId: session.gameId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const session = this.createFromForm();
    if (session.id !== undefined) {
      this.subscribeToSaveResponse(this.sessionService.update(session));
    } else {
      this.subscribeToSaveResponse(this.sessionService.create(session));
    }
  }

  private createFromForm(): ISession {
    return {
      ...new Session(),
      id: this.editForm.get(['id'])!.value,
      code: this.editForm.get(['code'])!.value,
      name: this.editForm.get(['name'])!.value,
      state: this.editForm.get(['state'])!.value,
      dateCreated: this.editForm.get(['dateCreated'])!.value
        ? moment(this.editForm.get(['dateCreated'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModified: this.editForm.get(['dateModified'])!.value
        ? moment(this.editForm.get(['dateModified'])!.value, DATE_TIME_FORMAT)
        : undefined,
      langKey: this.editForm.get(['langKey'])!.value,
      maxPlayers: this.editForm.get(['maxPlayers'])!.value,
      gameId: this.editForm.get(['gameId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISession>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IGame): any {
    return item.id;
  }
}
