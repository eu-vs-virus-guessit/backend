import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackendSharedModule } from 'app/shared/shared.module';
import { GuessComponent } from './guess.component';
import { GuessDetailComponent } from './guess-detail.component';
import { GuessUpdateComponent } from './guess-update.component';
import { GuessDeleteDialogComponent } from './guess-delete-dialog.component';
import { guessRoute } from './guess.route';

@NgModule({
  imports: [BackendSharedModule, RouterModule.forChild(guessRoute)],
  declarations: [GuessComponent, GuessDetailComponent, GuessUpdateComponent, GuessDeleteDialogComponent],
  entryComponents: [GuessDeleteDialogComponent]
})
export class BackendGuessModule {}
