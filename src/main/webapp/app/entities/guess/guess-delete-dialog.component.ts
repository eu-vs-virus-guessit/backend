import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IGuess } from 'app/shared/model/guess.model';
import { GuessService } from './guess.service';

@Component({
  templateUrl: './guess-delete-dialog.component.html'
})
export class GuessDeleteDialogComponent {
  guess?: IGuess;

  constructor(protected guessService: GuessService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.guessService.delete(id).subscribe(() => {
      this.eventManager.broadcast('guessListModification');
      this.activeModal.close();
    });
  }
}
