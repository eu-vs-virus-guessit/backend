import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGuess } from 'app/shared/model/guess.model';

@Component({
  selector: 'jhi-guess-detail',
  templateUrl: './guess-detail.component.html'
})
export class GuessDetailComponent implements OnInit {
  guess: IGuess | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ guess }) => (this.guess = guess));
  }

  previousState(): void {
    window.history.back();
  }
}
