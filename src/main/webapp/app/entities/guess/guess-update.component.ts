import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IGuess, Guess } from 'app/shared/model/guess.model';
import { GuessService } from './guess.service';
import { IRound } from 'app/shared/model/round.model';
import { RoundService } from 'app/entities/round/round.service';
import { IPlayer } from 'app/shared/model/player.model';
import { PlayerService } from 'app/entities/player/player.service';

type SelectableEntity = IRound | IPlayer;

@Component({
  selector: 'jhi-guess-update',
  templateUrl: './guess-update.component.html'
})
export class GuessUpdateComponent implements OnInit {
  isSaving = false;
  rounds: IRound[] = [];
  players: IPlayer[] = [];

  editForm = this.fb.group({
    id: [],
    content: [null, [Validators.required]],
    score: [null, [Validators.required]],
    type: [null, [Validators.required]],
    dateCreated: [null, [Validators.required]],
    dateModified: [null, [Validators.required]],
    roundId: [],
    playerId: []
  });

  constructor(
    protected guessService: GuessService,
    protected roundService: RoundService,
    protected playerService: PlayerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ guess }) => {
      if (!guess.id) {
        const today = moment().startOf('day');
        guess.dateCreated = today;
        guess.dateModified = today;
      }

      this.updateForm(guess);

      this.roundService.query().subscribe((res: HttpResponse<IRound[]>) => (this.rounds = res.body || []));

      this.playerService.query().subscribe((res: HttpResponse<IPlayer[]>) => (this.players = res.body || []));
    });
  }

  updateForm(guess: IGuess): void {
    this.editForm.patchValue({
      id: guess.id,
      content: guess.content,
      score: guess.score,
      type: guess.type,
      dateCreated: guess.dateCreated ? guess.dateCreated.format(DATE_TIME_FORMAT) : null,
      dateModified: guess.dateModified ? guess.dateModified.format(DATE_TIME_FORMAT) : null,
      roundId: guess.roundId,
      playerId: guess.playerId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const guess = this.createFromForm();
    if (guess.id !== undefined) {
      this.subscribeToSaveResponse(this.guessService.update(guess));
    } else {
      this.subscribeToSaveResponse(this.guessService.create(guess));
    }
  }

  private createFromForm(): IGuess {
    return {
      ...new Guess(),
      id: this.editForm.get(['id'])!.value,
      content: this.editForm.get(['content'])!.value,
      score: this.editForm.get(['score'])!.value,
      type: this.editForm.get(['type'])!.value,
      dateCreated: this.editForm.get(['dateCreated'])!.value
        ? moment(this.editForm.get(['dateCreated'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModified: this.editForm.get(['dateModified'])!.value
        ? moment(this.editForm.get(['dateModified'])!.value, DATE_TIME_FORMAT)
        : undefined,
      roundId: this.editForm.get(['roundId'])!.value,
      playerId: this.editForm.get(['playerId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGuess>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
