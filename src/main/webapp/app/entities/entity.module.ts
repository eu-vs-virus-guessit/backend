import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'game',
        loadChildren: () => import('./game/game.module').then(m => m.BackendGameModule)
      },
      {
        path: 'player',
        loadChildren: () => import('./player/player.module').then(m => m.BackendPlayerModule)
      },
      {
        path: 'guess',
        loadChildren: () => import('./guess/guess.module').then(m => m.BackendGuessModule)
      },
      {
        path: 'session',
        loadChildren: () => import('./session/session.module').then(m => m.BackendSessionModule)
      },
      {
        path: 'round',
        loadChildren: () => import('./round/round.module').then(m => m.BackendRoundModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class BackendEntityModule {}
