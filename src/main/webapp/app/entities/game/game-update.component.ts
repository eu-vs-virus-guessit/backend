import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IGame, Game } from 'app/shared/model/game.model';
import { GameService } from './game.service';

@Component({
  selector: 'jhi-game-update',
  templateUrl: './game-update.component.html'
})
export class GameUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    maxPlayers: [null, [Validators.required]],
    state: [null, [Validators.required]],
    numberOfRounds: [null, [Validators.required]],
    dateCreated: [null, [Validators.required]],
    dateModified: [null, [Validators.required]]
  });

  constructor(protected gameService: GameService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ game }) => {
      if (!game.id) {
        const today = moment().startOf('day');
        game.dateCreated = today;
        game.dateModified = today;
      }

      this.updateForm(game);
    });
  }

  updateForm(game: IGame): void {
    this.editForm.patchValue({
      id: game.id,
      maxPlayers: game.maxPlayers,
      state: game.state,
      numberOfRounds: game.numberOfRounds,
      dateCreated: game.dateCreated ? game.dateCreated.format(DATE_TIME_FORMAT) : null,
      dateModified: game.dateModified ? game.dateModified.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const game = this.createFromForm();
    if (game.id !== undefined) {
      this.subscribeToSaveResponse(this.gameService.update(game));
    } else {
      this.subscribeToSaveResponse(this.gameService.create(game));
    }
  }

  private createFromForm(): IGame {
    return {
      ...new Game(),
      id: this.editForm.get(['id'])!.value,
      maxPlayers: this.editForm.get(['maxPlayers'])!.value,
      state: this.editForm.get(['state'])!.value,
      numberOfRounds: this.editForm.get(['numberOfRounds'])!.value,
      dateCreated: this.editForm.get(['dateCreated'])!.value
        ? moment(this.editForm.get(['dateCreated'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModified: this.editForm.get(['dateModified'])!.value
        ? moment(this.editForm.get(['dateModified'])!.value, DATE_TIME_FORMAT)
        : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGame>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
