/**
 * View Models used by Spring MVC REST controllers.
 */
package org.casualcode.guessit.backend.web.rest.vm;
