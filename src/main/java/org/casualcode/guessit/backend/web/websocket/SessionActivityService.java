package org.casualcode.guessit.backend.web.websocket;

import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

@Controller
public class SessionActivityService {
    private static final Logger log = LoggerFactory.getLogger(ActivityService.class);

    private final SimpMessageSendingOperations messagingTemplate;

    public SessionActivityService(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @MessageMapping("/socket/session/{sessionCode}")
    public void sendActivity(@Payload SessionDTO sessionDTO, @DestinationVariable String sessionCode) {
        log.debug("Sending session data {}", sessionDTO);
        messagingTemplate.convertAndSend("/topic/session/track/" + sessionCode, sessionDTO);
    }


}
