package org.casualcode.guessit.backend.web.rest;

import com.netflix.ribbon.proxy.annotation.Http;
import org.casualcode.guessit.backend.domain.Session;
import org.casualcode.guessit.backend.domain.enumeration.SessionInfoState;
import org.casualcode.guessit.backend.domain.enumeration.SessionState;
import org.casualcode.guessit.backend.security.jwt.JWTToken;
import org.casualcode.guessit.backend.service.AuthenticationService;
import org.casualcode.guessit.backend.service.PlayerService;
import org.casualcode.guessit.backend.service.SessionService;
import org.casualcode.guessit.backend.service.UserService;
import org.casualcode.guessit.backend.service.dto.JoinSessionResponseDTO;
import org.casualcode.guessit.backend.service.dto.PlayerDTO;
import org.casualcode.guessit.backend.web.rest.errors.BadRequestAlertException;
import org.casualcode.guessit.backend.service.dto.SessionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.casualcode.guessit.backend.web.rest.vm.ManagedUserVM;
import org.casualcode.guessit.backend.web.websocket.SessionActivityService;
import org.checkerframework.checker.nullness.Opt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link org.casualcode.guessit.backend.domain.Session}.
 */
@RestController
@RequestMapping("/api")
public class SessionResource {

    private final Logger log = LoggerFactory.getLogger(SessionResource.class);

    private static final String ENTITY_NAME = "session";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SessionService sessionService;

    private final AuthenticationService authenticationService;

    private final PlayerService playerService;

    private final UserService userService;

    private final SessionActivityService sessionActivityService;

    public SessionResource(
        SessionService sessionService,
        AuthenticationService authenticationService,
        PlayerService playerService,
        UserService userService,
        SessionActivityService sessionActivityService
    ) {
        this.sessionService = sessionService;
        this.authenticationService = authenticationService;
        this.playerService = playerService;
        this.userService = userService;
        this.sessionActivityService = sessionActivityService;
    }

    /**
     * {@code POST  /sessions} : Create a new session.
     *
     * @param sessionDTO the sessionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sessionDTO, or with status {@code 400 (Bad Request)} if the session has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sessions")
    public ResponseEntity<SessionDTO> createSession(@Valid @RequestBody SessionDTO sessionDTO) throws Exception {
        log.debug("REST request to save Session : {}", sessionDTO);
        if (sessionDTO.getId() != null) {
            throw new BadRequestAlertException("A new session cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SessionDTO result = sessionService.save(sessionDTO);
        return ResponseEntity.created(new URI("/api/sessions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sessions} : Updates an existing session.
     *
     * @param sessionDTO the sessionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sessionDTO,
     * or with status {@code 400 (Bad Request)} if the sessionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sessionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sessions")
    public ResponseEntity<SessionDTO> updateSession(@Valid @RequestBody SessionDTO sessionDTO) throws Exception {
        log.debug("REST request to update Session : {}", sessionDTO);
        if (sessionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SessionDTO result = sessionService.save(sessionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sessionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sessions/:id} : get the "id" session.
     *
     * @param id the id of the sessionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sessionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sessions/{id}")
    public ResponseEntity<SessionDTO> getSession(@PathVariable Long id) {
        log.debug("REST request to get Session : {}", id);
        Optional<SessionDTO> sessionDTO = sessionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sessionDTO);
    }

    @GetMapping("/session/{sessionCode}/status")
    public ResponseEntity<SessionInfoState> isSessionValid(@PathVariable String sessionCode) {
        return new ResponseEntity<>(sessionService.isSessionValid(sessionCode), HttpStatus.OK);
    }

    @GetMapping("/session/{sessionCode}/join")
    public ResponseEntity<JoinSessionResponseDTO> joinSession(
        @PathVariable String sessionCode,
        @RequestParam String username
    ) {
        SessionInfoState sessionInfoState = sessionService.isSessionValid(sessionCode);
        if (sessionInfoState.equals(SessionInfoState.INVALID)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (sessionInfoState.equals(SessionInfoState.FULL) || sessionInfoState.equals(SessionInfoState.GAME_IN_PROGRESS)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Optional<SessionDTO> sessionDtoOptional = sessionService.findSessionBySessionCode(sessionCode);
        // TODO add constraint that not more than maxPlayers are allowed
        if (sessionDtoOptional.isPresent()) {
            SessionDTO sessionDTO = sessionDtoOptional.get();
            PlayerDTO playerDTO = playerService.createGuestPlayer(username, sessionDTO);
            sessionDTO.addPlayer(playerDTO);
            try {
                sessionDTO = sessionService.save(sessionDTO);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            sessionActivityService.sendActivity(sessionDTO, sessionCode);

            ManagedUserVM managedUserVM = authenticationService.buildManagedUserVm(playerDTO.getName() + sessionDTO.getId());
            userService.registerUser(managedUserVM, playerDTO.getName() + sessionDTO.getId());
            JWTToken jwtToken =
                authenticationService.authenticate(
                    playerDTO.getName() + sessionDTO.getId(),
                    playerDTO.getName() + sessionDTO.getId());
            JoinSessionResponseDTO response = new JoinSessionResponseDTO(sessionDTO, jwtToken, playerDTO);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/session/{sessionCode}/leave")
    public ResponseEntity<HttpStatus> leaveSession(
        @PathVariable String sessionCode
    ) {
        Optional<SessionDTO> sessionDTOOptional = sessionService.findSessionBySessionCode(sessionCode);
        if (sessionDTOOptional.isPresent()) {
            SessionDTO sessionDTO = sessionDTOOptional.get();
            Optional<PlayerDTO> playerDTOOptional = playerService.getMyPlayer(sessionDTO);
            if (playerDTOOptional.isPresent()) {

                PlayerDTO playerDTO = playerDTOOptional.get();
                sessionDTO.setPlayers(sessionDTO.getPlayers()
                    .stream()
                    .filter(player -> !player.getId().equals(playerDTO.getId())).collect(Collectors.toList()));
                if(playerDTO.isAdmin()){
                    sessionDTO.setState(SessionState.SESSION_ABORTED);
                }
                playerService.delete(playerDTO.getId());
                try {
                    sessionDTO = sessionService.save(sessionDTO);
                } catch (Exception e) {
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
                sessionActivityService.sendActivity(sessionDTO, sessionCode);
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * {@code DELETE  /sessions/:id} : delete the "id" session.
     *
     * @param id the id of the sessionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sessions/{id}")
    public ResponseEntity<Void> deleteSession(@PathVariable Long id) {
        log.debug("REST request to delete Session : {}", id);
        sessionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
