/**
 * Data Access Objects used by WebSocket services.
 */
package org.casualcode.guessit.backend.web.websocket.dto;
