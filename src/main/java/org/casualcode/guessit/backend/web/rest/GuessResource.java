package org.casualcode.guessit.backend.web.rest;

import org.casualcode.guessit.backend.service.GuessService;
import org.casualcode.guessit.backend.web.rest.errors.BadRequestAlertException;
import org.casualcode.guessit.backend.service.dto.GuessDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.casualcode.guessit.backend.domain.Guess}.
 */
@RestController
@RequestMapping("/api")
public class GuessResource {

    private final Logger log = LoggerFactory.getLogger(GuessResource.class);

    private static final String ENTITY_NAME = "guess";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GuessService guessService;

    public GuessResource(GuessService guessService) {
        this.guessService = guessService;
    }

    /**
     * {@code POST  /guesses} : Create a new guess.
     *
     * @param guessDTO the guessDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new guessDTO, or with status {@code 400 (Bad Request)} if the guess has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/guesses")
    public ResponseEntity<GuessDTO> createGuess(@Valid @RequestBody GuessDTO guessDTO) throws URISyntaxException {
        log.debug("REST request to save Guess : {}", guessDTO);
        if (guessDTO.getId() != null) {
            throw new BadRequestAlertException("A new guess cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GuessDTO result = guessService.save(guessDTO);
        return ResponseEntity.created(new URI("/api/guesses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /guesses} : Updates an existing guess.
     *
     * @param guessDTO the guessDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated guessDTO,
     * or with status {@code 400 (Bad Request)} if the guessDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the guessDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/guesses")
    public ResponseEntity<GuessDTO> updateGuess(@Valid @RequestBody GuessDTO guessDTO) throws URISyntaxException {
        log.debug("REST request to update Guess : {}", guessDTO);
        if (guessDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GuessDTO result = guessService.save(guessDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, guessDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /guesses} : get all the guesses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of guesses in body.
     */
    @GetMapping("/guesses")
    public ResponseEntity<List<GuessDTO>> getAllGuesses(Pageable pageable) {
        log.debug("REST request to get a page of Guesses");
        Page<GuessDTO> page = guessService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /guesses/:id} : get the "id" guess.
     *
     * @param id the id of the guessDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the guessDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/guesses/{id}")
    public ResponseEntity<GuessDTO> getGuess(@PathVariable Long id) {
        log.debug("REST request to get Guess : {}", id);
        Optional<GuessDTO> guessDTO = guessService.findOne(id);
        return ResponseUtil.wrapOrNotFound(guessDTO);
    }

    /**
     * {@code DELETE  /guesses/:id} : delete the "id" guess.
     *
     * @param id the id of the guessDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/guesses/{id}")
    public ResponseEntity<Void> deleteGuess(@PathVariable Long id) {
        log.debug("REST request to delete Guess : {}", id);
        guessService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
