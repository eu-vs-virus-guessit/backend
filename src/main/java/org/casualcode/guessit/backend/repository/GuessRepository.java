package org.casualcode.guessit.backend.repository;

import org.casualcode.guessit.backend.domain.Guess;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Guess entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GuessRepository extends JpaRepository<Guess, Long> {
}
