package org.casualcode.guessit.backend.repository;

import org.casualcode.guessit.backend.domain.Player;

import org.casualcode.guessit.backend.domain.Session;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Player entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

    Optional<Player> findByNameAndAdmin(String loginName, boolean admin);

    Optional<Player> findByName(String loginName);

    Optional<Player> findByNameAndSession(String loginName, Session session);
}
