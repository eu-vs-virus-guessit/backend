package org.casualcode.guessit.backend.service.dto;

import org.casualcode.guessit.backend.security.jwt.JWTToken;

public class JoinSessionResponseDTO {
    SessionDTO session;
    JWTToken token;
    PlayerDTO playerDTO;

    public JoinSessionResponseDTO(SessionDTO session, JWTToken token, PlayerDTO playerDTO) {
        this.session = session;
        this.token = token;
        this.playerDTO = playerDTO;
    }

    public SessionDTO getSession() {
        return session;
    }

    public void setSession(SessionDTO session) {
        this.session = session;
    }

    public JWTToken getToken() {
        return token;
    }

    public void setToken(JWTToken token) {
        this.token = token;
    }

    public PlayerDTO getPlayerDTO() {
        return playerDTO;
    }

    public void setPlayerDTO(PlayerDTO playerDTO) {
        this.playerDTO = playerDTO;
    }
}
