package org.casualcode.guessit.backend.service.mapper;


import org.casualcode.guessit.backend.domain.*;
import org.casualcode.guessit.backend.service.dto.GuessDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Guess} and its DTO {@link GuessDTO}.
 */
@Mapper(componentModel = "spring", uses = {RoundMapper.class, PlayerMapper.class})
public interface GuessMapper extends EntityMapper<GuessDTO, Guess> {

    @Mapping(source = "round.id", target = "roundId")
    @Mapping(source = "player.id", target = "playerId")
    GuessDTO toDto(Guess guess);

    @Mapping(source = "roundId", target = "round")
    @Mapping(source = "playerId", target = "player")
    Guess toEntity(GuessDTO guessDTO);

    default Guess fromId(Long id) {
        if (id == null) {
            return null;
        }
        Guess guess = new Guess();
        guess.setId(id);
        return guess;
    }
}
