package org.casualcode.guessit.backend.service;

import org.casualcode.guessit.backend.domain.enumeration.SessionInfoState;
import org.casualcode.guessit.backend.service.dto.SessionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link org.casualcode.guessit.backend.domain.Session}.
 */
public interface SessionService {

    /**
     * Save a session.
     *
     * @param sessionDTO the entity to save.
     * @return the persisted entity.
     */
    SessionDTO save(SessionDTO sessionDTO) throws Exception;

    /**
     * Get all the sessions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SessionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" session.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SessionDTO> findOne(Long id);

    /**
     * Delete the "id" session.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    boolean doesSessionExist(String sessionCode);

    Optional<SessionDTO> findSessionBySessionCode(String sessionCode);

    SessionInfoState isSessionValid(String sessionCode);
}
