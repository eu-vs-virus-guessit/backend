package org.casualcode.guessit.backend.service.mapper;


import org.casualcode.guessit.backend.domain.*;
import org.casualcode.guessit.backend.service.dto.GameDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Game} and its DTO {@link GameDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GameMapper extends EntityMapper<GameDTO, Game> {


    @Mapping(target = "rounds", ignore = true)
    @Mapping(target = "removeRound", ignore = true)
    @Mapping(target = "session", ignore = true)
    Game toEntity(GameDTO gameDTO);

    default Game fromId(Long id) {
        if (id == null) {
            return null;
        }
        Game game = new Game();
        game.setId(id);
        return game;
    }
}
