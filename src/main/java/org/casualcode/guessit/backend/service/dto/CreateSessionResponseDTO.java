package org.casualcode.guessit.backend.service.dto;

public class CreateSessionResponseDTO {
    SessionDTO session;
    PlayerDTO player;

    public CreateSessionResponseDTO(SessionDTO session, PlayerDTO player) {
        this.session = session;
        this.player = player;
    }

    public SessionDTO getSession() {
        return session;
    }

    public void setSession(SessionDTO session) {
        this.session = session;
    }

    public PlayerDTO getPlayer() {
        return player;
    }

    public void setPlayer(PlayerDTO player) {
        this.player = player;
    }
}
