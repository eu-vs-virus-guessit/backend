package org.casualcode.guessit.backend.service.impl;

import org.casualcode.guessit.backend.service.PlayerService;
import org.casualcode.guessit.backend.domain.Player;
import org.casualcode.guessit.backend.repository.PlayerRepository;
import org.casualcode.guessit.backend.service.dto.PlayerDTO;
import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.casualcode.guessit.backend.service.mapper.PlayerMapper;
import org.casualcode.guessit.backend.service.mapper.SessionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Player}.
 */
@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

    private final Logger log = LoggerFactory.getLogger(PlayerServiceImpl.class);

    private final PlayerRepository playerRepository;

    private final PlayerMapper playerMapper;

    private final SessionMapper sessionMapper;


    public PlayerServiceImpl(PlayerRepository playerRepository, PlayerMapper playerMapper, SessionMapper sessionMapper) {
        this.playerRepository = playerRepository;
        this.playerMapper = playerMapper;
        this.sessionMapper = sessionMapper;
    }

    /**
     * Save a player.
     *
     * @param playerDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PlayerDTO save(PlayerDTO playerDTO) {
        log.debug("Request to save Player : {}", playerDTO);
        Player player = playerMapper.toEntity(playerDTO);
        player = playerRepository.save(player);
        return playerMapper.toDto(player);
    }

    /**
     * Get all the players.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PlayerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Players");
        return playerRepository.findAll(pageable)
            .map(playerMapper::toDto);
    }

    /**
     * Get one player by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PlayerDTO> findOne(Long id) {
        log.debug("Request to get Player : {}", id);
        return playerRepository.findById(id)
            .map(playerMapper::toDto);
    }

    /**
     * Delete the player by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Player : {}", id);
        playerRepository.deleteById(id);
    }

    @Override
    public Optional<PlayerDTO> findByLoginNameAndAdminStatus(String loginName, boolean adminFlag) {
        return playerRepository.findByNameAndAdmin(loginName, adminFlag).map(playerMapper::toDto);
    }

    @Override
    public Optional<PlayerDTO> findByLoginName(String loginName) {
        return playerRepository.findByName(loginName).map(playerMapper::toDto);
    }

    @Override
    public PlayerDTO createGuestPlayer(String username, SessionDTO sessionDTO) {
        PlayerDTO playerDTO = new PlayerDTO();
        playerDTO.setName(username);
        playerDTO.setAdmin(false);
        playerDTO.setDateCreated(Instant.now());
        playerDTO.setDateModified(Instant.now());
        playerDTO.setSessionId(sessionDTO.getId());
        return playerMapper.toDto(playerRepository.save(playerMapper.toEntity(playerDTO)));
    }

    @Override
    public Optional<PlayerDTO> getMyPlayer(SessionDTO sessionDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            User user = (User) authentication.getPrincipal();
            Optional<Player> playerOptional =
                playerRepository.findByNameAndSession(
                    user.getUsername().replace(sessionDTO.getId().toString(), ""),
                    sessionMapper.toEntity(sessionDTO));
            return playerOptional.map(playerMapper::toDto);
        }
        return Optional.empty();
    }

}
