package org.casualcode.guessit.backend.service.impl;

import org.casualcode.guessit.backend.domain.enumeration.SessionInfoState;
import org.casualcode.guessit.backend.domain.enumeration.SessionState;
import org.casualcode.guessit.backend.service.exception.SessionAlreadyExistsException;
import org.casualcode.guessit.backend.service.SessionService;
import org.casualcode.guessit.backend.domain.Session;
import org.casualcode.guessit.backend.repository.SessionRepository;
import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.casualcode.guessit.backend.service.mapper.CustomSessionMapper;
import org.casualcode.guessit.backend.service.mapper.SessionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Session}.
 */
@Service
@Transactional
public class SessionServiceImpl implements SessionService {

    private final Logger log = LoggerFactory.getLogger(SessionServiceImpl.class);

    private final SessionRepository sessionRepository;

    private final SessionMapper sessionMapper;

    private final CustomSessionMapper customSessionMapper;

    public SessionServiceImpl(SessionRepository sessionRepository, SessionMapper sessionMapper, CustomSessionMapper customSessionMapper) {
        this.sessionRepository = sessionRepository;
        this.sessionMapper = sessionMapper;
        this.customSessionMapper = customSessionMapper;
    }

    /**
     * Save a session.
     *
     * @param sessionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SessionDTO save(SessionDTO sessionDTO) throws SessionAlreadyExistsException {
        log.debug("Request to save Session : {}", sessionDTO);
        Session session = sessionMapper.toEntity(sessionDTO);
        session.setDateModified(Instant.now());
        session = sessionRepository.save(session);
        return sessionMapper.toDto(session);
    }

    /**
     * Get all the sessions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SessionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Sessions");
        return sessionRepository.findAll(pageable)
            .map(customSessionMapper::toDTO);
    }

    /**
     * Get one session by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SessionDTO> findOne(Long id) {
        log.debug("Request to get Session : {}", id);
        return sessionRepository.findById(id).map(customSessionMapper::toDTO);
    }

    /**
     * Delete the session by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Session : {}", id);
        sessionRepository.deleteById(id);
    }

    @Override
    public boolean doesSessionExist(String sessionCode) {
        return sessionRepository.findByCode(sessionCode).isPresent();
    }

    @Override
    public Optional<SessionDTO> findSessionBySessionCode(String sessionCode) {
        return sessionRepository.findByCode(sessionCode).map(sessionMapper::toDto);
    }

    @Override
    public SessionInfoState isSessionValid(String sessionCode) {
        Optional<SessionDTO> sessionDtoOptional = findSessionBySessionCode(sessionCode);
        if (sessionDtoOptional.isPresent()) {
            SessionDTO sessionDTO = sessionDtoOptional.get();
            if(sessionDTO.getState().equals(SessionState.LOBBY)){
                if(sessionDTO.getPlayers().size() < sessionDTO.getMaxPlayers()){
                    return SessionInfoState.OPEN;
                } else {
                    return SessionInfoState.FULL;
                }
            } else if(sessionDTO.getState().equals(SessionState.GAME_IN_PROGRESS)){
                return SessionInfoState.GAME_IN_PROGRESS;
            }
        }
        return SessionInfoState.INVALID;
    }
}
