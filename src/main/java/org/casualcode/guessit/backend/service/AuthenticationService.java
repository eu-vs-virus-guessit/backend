package org.casualcode.guessit.backend.service;

import org.casualcode.guessit.backend.security.jwt.JWTToken;
import org.casualcode.guessit.backend.service.dto.CreateSessionRequestDTO;
import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.casualcode.guessit.backend.web.rest.vm.ManagedUserVM;

public interface AuthenticationService {

    JWTToken authenticate(String userName, String password);

    ManagedUserVM buildManagedUserVm(String userName);

    SessionDTO createPlayerAndSession(CreateSessionRequestDTO createSessionRequestDto) throws Exception;
}
