package org.casualcode.guessit.backend.service.impl;

import org.casualcode.guessit.backend.domain.enumeration.SessionState;
import org.casualcode.guessit.backend.security.AuthoritiesConstants;
import org.casualcode.guessit.backend.security.jwt.JWTToken;
import org.casualcode.guessit.backend.security.jwt.TokenProvider;
import org.casualcode.guessit.backend.service.AuthenticationService;
import org.casualcode.guessit.backend.service.PlayerService;
import org.casualcode.guessit.backend.service.SessionService;
import org.casualcode.guessit.backend.service.dto.PlayerDTO;
import org.casualcode.guessit.backend.service.dto.CreateSessionRequestDTO;
import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.casualcode.guessit.backend.web.rest.vm.ManagedUserVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final Logger log = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final TokenProvider tokenProvider;

    private final SessionService sessionService;

    private final PlayerService playerService;

    public AuthenticationServiceImpl(AuthenticationManagerBuilder authenticationManagerBuilder, TokenProvider tokenProvider, SessionService sessionService, PlayerService playerService) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.tokenProvider = tokenProvider;
        this.sessionService = sessionService;
        this.playerService = playerService;
    }

    @Override
    public JWTToken authenticate(String userName, String password) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(userName, password);

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String tokenAsString = tokenProvider.createToken(authentication, false);

        return new JWTToken(tokenAsString);
    }

    @Override
    public ManagedUserVM buildManagedUserVm(String userName) {
        ManagedUserVM managedUserVm = new ManagedUserVM();
        managedUserVm.setActivated(true);
        managedUserVm.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));
        managedUserVm.setEmail(userName + "@casualcode.org");
        managedUserVm.setLogin(userName);
        managedUserVm.setPassword(userName);

        return managedUserVm;
    }

    @Override
    public SessionDTO createPlayerAndSession(CreateSessionRequestDTO createSessionRequestDto) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        Optional<SessionDTO> optionalSessionDTO =
            sessionService.findSessionBySessionCode(createSessionRequestDto.getSessionCode());

        if(optionalSessionDTO.isEmpty()){
            SessionDTO sessionDTO = new SessionDTO();
            sessionDTO.setCode(createSessionRequestDto.getSessionCode());
            sessionDTO.setDateCreated(Instant.now());
            sessionDTO.setDateModified(Instant.now());
            sessionDTO.setName(createSessionRequestDto.getSessionName());
            sessionDTO.setState(SessionState.LOBBY);
            sessionDTO.setMaxPlayers(createSessionRequestDto.getMaxPlayers());
            sessionDTO.setLangKey(createSessionRequestDto.getLanguage());
            sessionDTO.setPlayers(new ArrayList<>());

            User user = (User) authentication.getPrincipal();

            PlayerDTO playerDTO = new PlayerDTO();
            playerDTO.setAdmin(true);
            playerDTO.setName(user.getUsername());
            playerDTO.setDateCreated(Instant.now());
            playerDTO.setDateModified(Instant.now());

            sessionDTO.addPlayer(playerDTO);

            sessionDTO = sessionService.save(sessionDTO);

            playerDTO.setSessionId(sessionDTO.getId());
            playerService.save(playerDTO);
            return sessionDTO;
        } else {
            throw new EntityExistsException();
        }
    }


}
