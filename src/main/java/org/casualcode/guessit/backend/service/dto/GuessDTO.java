package org.casualcode.guessit.backend.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import org.casualcode.guessit.backend.domain.enumeration.GuessType;

/**
 * A DTO for the {@link org.casualcode.guessit.backend.domain.Guess} entity.
 */
public class GuessDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String content;

    @NotNull
    private Integer score;

    @NotNull
    private GuessType type;

    @NotNull
    private Instant dateCreated;

    @NotNull
    private Instant dateModified;


    private Long roundId;

    private Long playerId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public GuessType getType() {
        return type;
    }

    public void setType(GuessType type) {
        this.type = type;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    public void setDateModified(Instant dateModified) {
        this.dateModified = dateModified;
    }

    public Long getRoundId() {
        return roundId;
    }

    public void setRoundId(Long roundId) {
        this.roundId = roundId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GuessDTO guessDTO = (GuessDTO) o;
        if (guessDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), guessDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GuessDTO{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", score=" + getScore() +
            ", type='" + getType() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", dateModified='" + getDateModified() + "'" +
            ", roundId=" + getRoundId() +
            ", playerId=" + getPlayerId() +
            "}";
    }
}
