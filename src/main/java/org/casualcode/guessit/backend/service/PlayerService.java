package org.casualcode.guessit.backend.service;

import org.casualcode.guessit.backend.service.dto.PlayerDTO;

import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link org.casualcode.guessit.backend.domain.Player}.
 */
public interface PlayerService {

    /**
     * Save a player.
     *
     * @param playerDTO the entity to save.
     * @return the persisted entity.
     */
    PlayerDTO save(PlayerDTO playerDTO);

    /**
     * Get all the players.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PlayerDTO> findAll(Pageable pageable);

    /**
     * Get the "id" player.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PlayerDTO> findOne(Long id);

    /**
     * Delete the "id" player.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<PlayerDTO> findByLoginNameAndAdminStatus(String loginName, boolean admin);

    Optional<PlayerDTO> findByLoginName(String loginName);

    PlayerDTO createGuestPlayer(String username, SessionDTO sessionDTO);

    Optional<PlayerDTO> getMyPlayer(SessionDTO sessionDTO) ;
}
