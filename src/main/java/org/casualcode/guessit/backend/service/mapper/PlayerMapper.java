package org.casualcode.guessit.backend.service.mapper;


import org.casualcode.guessit.backend.domain.*;
import org.casualcode.guessit.backend.service.dto.PlayerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Player} and its DTO {@link PlayerDTO}.
 */
@Mapper(componentModel = "spring", uses = {SessionMapper.class})
public interface PlayerMapper extends EntityMapper<PlayerDTO, Player> {

    @Mapping(source = "session.id", target = "sessionId")
    PlayerDTO toDto(Player player);

    @Mapping(target = "guesses", ignore = true)
    @Mapping(target = "removeGuess", ignore = true)
    @Mapping(source = "sessionId", target = "session")
    Player toEntity(PlayerDTO playerDTO);

    default Player fromId(Long id) {
        if (id == null) {
            return null;
        }
        Player player = new Player();
        player.setId(id);
        return player;
    }
}
