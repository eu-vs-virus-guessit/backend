package org.casualcode.guessit.backend.service.exception;

public class SessionAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SessionAlreadyExistsException() {
        super("Session does already exist!");
    }
}
