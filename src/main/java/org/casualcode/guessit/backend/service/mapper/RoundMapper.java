package org.casualcode.guessit.backend.service.mapper;


import org.casualcode.guessit.backend.domain.*;
import org.casualcode.guessit.backend.service.dto.RoundDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Round} and its DTO {@link RoundDTO}.
 */
@Mapper(componentModel = "spring", uses = {GameMapper.class})
public interface RoundMapper extends EntityMapper<RoundDTO, Round> {

    @Mapping(source = "game.id", target = "gameId")
    RoundDTO toDto(Round round);

    @Mapping(target = "guesses", ignore = true)
    @Mapping(target = "removeGuess", ignore = true)
    @Mapping(source = "gameId", target = "game")
    Round toEntity(RoundDTO roundDTO);

    default Round fromId(Long id) {
        if (id == null) {
            return null;
        }
        Round round = new Round();
        round.setId(id);
        return round;
    }
}
