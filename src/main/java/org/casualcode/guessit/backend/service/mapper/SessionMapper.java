package org.casualcode.guessit.backend.service.mapper;


import org.casualcode.guessit.backend.domain.*;
import org.casualcode.guessit.backend.service.dto.SessionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Session} and its DTO {@link SessionDTO}.
 */
@Mapper(componentModel = "spring", uses = {GameMapper.class, PlayerMapper.class})
public interface SessionMapper extends EntityMapper<SessionDTO, Session> {

    @Mapping(source = "game.id", target = "gameId")
    SessionDTO toDto(Session session);

    @Mapping(source = "gameId", target = "game")
//    @Mapping(target = "players", ignore = true)
    @Mapping(target = "removePlayer", ignore = true)
    Session toEntity(SessionDTO sessionDTO);

    default Session fromId(Long id) {
        if (id == null) {
            return null;
        }
        Session session = new Session();
        session.setId(id);
        return session;
    }
}
