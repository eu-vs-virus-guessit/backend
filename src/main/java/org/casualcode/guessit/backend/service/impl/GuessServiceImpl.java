package org.casualcode.guessit.backend.service.impl;

import org.casualcode.guessit.backend.service.GuessService;
import org.casualcode.guessit.backend.domain.Guess;
import org.casualcode.guessit.backend.repository.GuessRepository;
import org.casualcode.guessit.backend.service.dto.GuessDTO;
import org.casualcode.guessit.backend.service.mapper.GuessMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Guess}.
 */
@Service
@Transactional
public class GuessServiceImpl implements GuessService {

    private final Logger log = LoggerFactory.getLogger(GuessServiceImpl.class);

    private final GuessRepository guessRepository;

    private final GuessMapper guessMapper;

    public GuessServiceImpl(GuessRepository guessRepository, GuessMapper guessMapper) {
        this.guessRepository = guessRepository;
        this.guessMapper = guessMapper;
    }

    /**
     * Save a guess.
     *
     * @param guessDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public GuessDTO save(GuessDTO guessDTO) {
        log.debug("Request to save Guess : {}", guessDTO);
        Guess guess = guessMapper.toEntity(guessDTO);
        guess = guessRepository.save(guess);
        return guessMapper.toDto(guess);
    }

    /**
     * Get all the guesses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GuessDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Guesses");
        return guessRepository.findAll(pageable)
            .map(guessMapper::toDto);
    }

    /**
     * Get one guess by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<GuessDTO> findOne(Long id) {
        log.debug("Request to get Guess : {}", id);
        return guessRepository.findById(id)
            .map(guessMapper::toDto);
    }

    /**
     * Delete the guess by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Guess : {}", id);
        guessRepository.deleteById(id);
    }
}
