package org.casualcode.guessit.backend.service;

import org.casualcode.guessit.backend.service.dto.GuessDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link org.casualcode.guessit.backend.domain.Guess}.
 */
public interface GuessService {

    /**
     * Save a guess.
     *
     * @param guessDTO the entity to save.
     * @return the persisted entity.
     */
    GuessDTO save(GuessDTO guessDTO);

    /**
     * Get all the guesses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GuessDTO> findAll(Pageable pageable);

    /**
     * Get the "id" guess.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GuessDTO> findOne(Long id);

    /**
     * Delete the "id" guess.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
