package org.casualcode.guessit.backend.service.impl;

import org.casualcode.guessit.backend.service.GameService;
import org.casualcode.guessit.backend.domain.Game;
import org.casualcode.guessit.backend.repository.GameRepository;
import org.casualcode.guessit.backend.service.RoundService;
import org.casualcode.guessit.backend.service.WordService;
import org.casualcode.guessit.backend.service.dto.GameDTO;
import org.casualcode.guessit.backend.service.dto.RoundDTO;
import org.casualcode.guessit.backend.service.mapper.GameMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link Game}.
 */
@Service
@Transactional
public class GameServiceImpl implements GameService {

    private final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    private final GameRepository gameRepository;

    private final GameMapper gameMapper;

    private final RoundService roundService;

    private final WordService wordService;

    public GameServiceImpl(GameRepository gameRepository, GameMapper gameMapper, RoundService roundService, WordService wordService) {
        this.gameRepository = gameRepository;
        this.gameMapper = gameMapper;
        this.roundService = roundService;
        this.wordService = wordService;
    }

    /**
     * Save a game.
     *
     * @param gameDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public GameDTO save(GameDTO gameDTO) {
        log.debug("Request to save Game : {}", gameDTO);
        Game game = gameMapper.toEntity(gameDTO);
        if (game.getDateCreated() == null) {
            game.setDateCreated(Instant.now());
        }
        game.setDateModified(Instant.now());
        game = gameRepository.save(game);
        return gameMapper.toDto(game);
    }

    /**
     * Get all the games.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GameDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Games");
        return gameRepository.findAll(pageable)
            .map(gameMapper::toDto);
    }


    /**
     *  Get all the games where Session is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<GameDTO> findAllWhereSessionIsNull() {
        log.debug("Request to get all games where Session is null");
        return StreamSupport
            .stream(gameRepository.findAll().spliterator(), false)
            .filter(game -> game.getSession() == null)
            .map(gameMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one game by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<GameDTO> findOne(Long id) {
        log.debug("Request to get Game : {}", id);
        return gameRepository.findById(id)
            .map(gameMapper::toDto);
    }

    /**
     * Delete the game by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Game : {}", id);
        gameRepository.deleteById(id);
    }

    @Override
    public void populateGameDTOWithRounds(GameDTO gameDTO, String langKey) throws IOException {
        log.debug("Populating Game {} with {} Rounds", gameDTO.getId(), gameDTO.getNumberOfRounds());
        for (int i = 0; i <= gameDTO.getNumberOfRounds(); i++) {
            String wordForRound = wordService.getWordForRound(langKey);
            RoundDTO roundDTO = new RoundDTO();
            roundDTO.setGameId(gameDTO.getId());
            roundDTO.setIteration(i);
            roundDTO.setWord(wordForRound);

            roundService.save(roundDTO);
        }
    }
}
