package org.casualcode.guessit.backend.service;

import org.casualcode.guessit.backend.service.dto.GameDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link org.casualcode.guessit.backend.domain.Game}.
 */
public interface GameService {

    /**
     * Save a game.
     *
     * @param gameDTO the entity to save.
     * @return the persisted entity.
     */
    GameDTO save(GameDTO gameDTO);

    /**
     * Get all the games.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GameDTO> findAll(Pageable pageable);
    /**
     * Get all the GameDTO where Session is {@code null}.
     *
     * @return the list of entities.
     */
    List<GameDTO> findAllWhereSessionIsNull();

    /**
     * Get the "id" game.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GameDTO> findOne(Long id);

    /**
     * Delete the "id" game.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void populateGameDTOWithRounds(GameDTO gameDTO, String langKey) throws IOException;
}
