package org.casualcode.guessit.backend.service;

import java.io.IOException;

public interface WordService {

    String getWordForRound(String langKey) throws IOException;
}
