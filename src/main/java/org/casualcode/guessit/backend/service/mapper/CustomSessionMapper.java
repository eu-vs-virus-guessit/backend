package org.casualcode.guessit.backend.service.mapper;

import org.casualcode.guessit.backend.domain.Player;
import org.casualcode.guessit.backend.domain.Session;
import org.casualcode.guessit.backend.service.dto.SessionDTO;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomSessionMapper {

    private final PlayerMapper playerMapper;

    public CustomSessionMapper(PlayerMapper playerMapper) {
        this.playerMapper = playerMapper;
    }

    public SessionDTO toDTO(Session session) {

            SessionDTO sessionDTO = new SessionDTO();
            sessionDTO.setGameId(1L);
            sessionDTO.setCode(session.getCode());
            sessionDTO.setName(session.getName());
            sessionDTO.setPlayers(session.getPlayers().stream().map(playerMapper::toDto).collect(Collectors.toList()));
            sessionDTO.setDateCreated(session.getDateCreated());
            sessionDTO.setDateModified(session.getDateModified());
            sessionDTO.setState(session.getState());
            sessionDTO.setId(session.getId());

            return sessionDTO;
    }
}
