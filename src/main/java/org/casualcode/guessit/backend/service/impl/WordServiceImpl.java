package org.casualcode.guessit.backend.service.impl;

import org.casualcode.guessit.backend.service.WordService;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

@Service
public class WordServiceImpl implements WordService {

    private final int MAX_LINES_IN_DICT = 90963;

    @Override
    public String getWordForRound(String langKey) throws IOException {
        Random random = new Random();
        int lineNumber = random.nextInt(MAX_LINES_IN_DICT);
        String word = "";

        try (Stream<String> lines = Files.lines(ResourceUtils.getFile("classpath:" + langKey + ".txt").toPath(), StandardCharsets.ISO_8859_1)) {
            Optional<String> optionalWord = lines.skip(lineNumber).findFirst();
            if (optionalWord.isPresent()) {
                word = optionalWord.get();
            } else {
                optionalWord = lines.skip(random.nextInt(100)).findFirst();
                if (optionalWord.isPresent()) {
                    word = optionalWord.get();
                }
            }
        }

        return word;
    }
}
