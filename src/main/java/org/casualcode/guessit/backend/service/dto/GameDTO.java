package org.casualcode.guessit.backend.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import org.casualcode.guessit.backend.domain.enumeration.GameState;

/**
 * A DTO for the {@link org.casualcode.guessit.backend.domain.Game} entity.
 */
public class GameDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer maxPlayers;

    @NotNull
    private GameState state;

    @NotNull
    private Integer numberOfRounds;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public Integer getNumberOfRounds() {
        return numberOfRounds;
    }

    public void setNumberOfRounds(Integer numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GameDTO gameDTO = (GameDTO) o;
        if (gameDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gameDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GameDTO{" +
            "id=" + getId() +
            ", maxPlayers=" + getMaxPlayers() +
            ", state='" + getState() + "'" +
            ", numberOfRounds=" + getNumberOfRounds() +
            "}";
    }
}
