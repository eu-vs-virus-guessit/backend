package org.casualcode.guessit.backend.domain.enumeration;

/**
 * The GameState enumeration.
 */
public enum GameState {
    THINK_PHASE, VOTE_PHASE, GUESSING_PHASE
}
