package org.casualcode.guessit.backend.domain.enumeration;

/**
 * The SessionState enumeration.
 */
public enum SessionState {
    LOBBY, GAME_IN_PROGRESS, GAME_ENDED, SESSION_ABORTED
}
