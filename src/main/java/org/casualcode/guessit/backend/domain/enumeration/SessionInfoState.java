package org.casualcode.guessit.backend.domain.enumeration;

public enum  SessionInfoState {
    OPEN, FULL, INVALID, GAME_IN_PROGRESS
}
