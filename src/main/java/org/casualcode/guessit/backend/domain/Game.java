package org.casualcode.guessit.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import org.casualcode.guessit.backend.domain.enumeration.GameState;

/**
 * A Game.
 */
@Entity
@Table(name = "game")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "max_players", nullable = false)
    private Integer maxPlayers;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private GameState state;

    @NotNull
    @Column(name = "number_of_rounds", nullable = false)
    private Integer numberOfRounds;

    @NotNull
    @Column(name = "date_created", nullable = false)
    private Instant dateCreated;

    @NotNull
    @Column(name = "date_modified", nullable = false)
    private Instant dateModified;

    @OneToMany(mappedBy = "game")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Round> rounds = new HashSet<>();

    @OneToOne(mappedBy = "game")
    @JsonIgnore
    private Session session;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public Game maxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
        return this;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public GameState getState() {
        return state;
    }

    public Game state(GameState state) {
        this.state = state;
        return this;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public Integer getNumberOfRounds() {
        return numberOfRounds;
    }

    public Game numberOfRounds(Integer numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
        return this;
    }

    public void setNumberOfRounds(Integer numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public Game dateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    public Game dateModified(Instant dateModified) {
        this.dateModified = dateModified;
        return this;
    }

    public void setDateModified(Instant dateModified) {
        this.dateModified = dateModified;
    }

    public Set<Round> getRounds() {
        return rounds;
    }

    public Game rounds(Set<Round> rounds) {
        this.rounds = rounds;
        return this;
    }

    public Game addRound(Round round) {
        this.rounds.add(round);
        round.setGame(this);
        return this;
    }

    public Game removeRound(Round round) {
        this.rounds.remove(round);
        round.setGame(null);
        return this;
    }

    public void setRounds(Set<Round> rounds) {
        this.rounds = rounds;
    }

    public Session getSession() {
        return session;
    }

    public Game session(Session session) {
        this.session = session;
        return this;
    }

    public void setSession(Session session) {
        this.session = session;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Game)) {
            return false;
        }
        return id != null && id.equals(((Game) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Game{" +
            "id=" + getId() +
            ", maxPlayers=" + getMaxPlayers() +
            ", state='" + getState() + "'" +
            ", numberOfRounds=" + getNumberOfRounds() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", dateModified='" + getDateModified() + "'" +
            "}";
    }
}
