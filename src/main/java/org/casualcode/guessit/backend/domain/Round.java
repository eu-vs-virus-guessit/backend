package org.casualcode.guessit.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Round.
 */
@Entity
@Table(name = "round")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Round implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "iteration", nullable = false)
    private Integer iteration;

    @NotNull
    @Column(name = "date_created", nullable = false)
    private Instant dateCreated;

    @NotNull
    @Column(name = "date_modified", nullable = false)
    private Instant dateModified;

    @NotNull
    @Column(name = "word", nullable = false)
    private String word;

    @OneToMany(mappedBy = "round")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Guess> guesses = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("rounds")
    private Game game;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIteration() {
        return iteration;
    }

    public Round iteration(Integer iteration) {
        this.iteration = iteration;
        return this;
    }

    public void setIteration(Integer iteration) {
        this.iteration = iteration;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public Round dateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Instant getDateModified() {
        return dateModified;
    }

    public Round dateModified(Instant dateModified) {
        this.dateModified = dateModified;
        return this;
    }

    public void setDateModified(Instant dateModified) {
        this.dateModified = dateModified;
    }

    public String getWord() {
        return word;
    }

    public Round word(String word) {
        this.word = word;
        return this;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Set<Guess> getGuesses() {
        return guesses;
    }

    public Round guesses(Set<Guess> guesses) {
        this.guesses = guesses;
        return this;
    }

    public Round addGuess(Guess guess) {
        this.guesses.add(guess);
        guess.setRound(this);
        return this;
    }

    public Round removeGuess(Guess guess) {
        this.guesses.remove(guess);
        guess.setRound(null);
        return this;
    }

    public void setGuesses(Set<Guess> guesses) {
        this.guesses = guesses;
    }

    public Game getGame() {
        return game;
    }

    public Round game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Round)) {
            return false;
        }
        return id != null && id.equals(((Round) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Round{" +
            "id=" + getId() +
            ", iteration=" + getIteration() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", dateModified='" + getDateModified() + "'" +
            ", word='" + getWord() + "'" +
            "}";
    }
}
