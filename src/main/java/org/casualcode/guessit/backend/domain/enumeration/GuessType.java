package org.casualcode.guessit.backend.domain.enumeration;

/**
 * The GuessType enumeration.
 */
public enum GuessType {
    DEFAULT, HINT
}
