package org.casualcode.guessit.backend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.casualcode.guessit.backend.web.rest.TestUtil;

public class GuessTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Guess.class);
        Guess guess1 = new Guess();
        guess1.setId(1L);
        Guess guess2 = new Guess();
        guess2.setId(guess1.getId());
        assertThat(guess1).isEqualTo(guess2);
        guess2.setId(2L);
        assertThat(guess1).isNotEqualTo(guess2);
        guess1.setId(null);
        assertThat(guess1).isNotEqualTo(guess2);
    }
}
