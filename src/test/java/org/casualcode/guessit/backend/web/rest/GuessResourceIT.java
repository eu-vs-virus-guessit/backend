package org.casualcode.guessit.backend.web.rest;

import org.casualcode.guessit.backend.BackendApp;
import org.casualcode.guessit.backend.domain.Guess;
import org.casualcode.guessit.backend.repository.GuessRepository;
import org.casualcode.guessit.backend.service.GuessService;
import org.casualcode.guessit.backend.service.dto.GuessDTO;
import org.casualcode.guessit.backend.service.mapper.GuessMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.casualcode.guessit.backend.domain.enumeration.GuessType;
/**
 * Integration tests for the {@link GuessResource} REST controller.
 */
@SpringBootTest(classes = BackendApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class GuessResourceIT {

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Integer DEFAULT_SCORE = 1;
    private static final Integer UPDATED_SCORE = 2;

    private static final GuessType DEFAULT_TYPE = GuessType.DEFAULT;
    private static final GuessType UPDATED_TYPE = GuessType.HINT;

    private static final Instant DEFAULT_DATE_CREATED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFIED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFIED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private GuessRepository guessRepository;

    @Autowired
    private GuessMapper guessMapper;

    @Autowired
    private GuessService guessService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGuessMockMvc;

    private Guess guess;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Guess createEntity(EntityManager em) {
        Guess guess = new Guess()
            .content(DEFAULT_CONTENT)
            .score(DEFAULT_SCORE)
            .type(DEFAULT_TYPE)
            .dateCreated(DEFAULT_DATE_CREATED)
            .dateModified(DEFAULT_DATE_MODIFIED);
        return guess;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Guess createUpdatedEntity(EntityManager em) {
        Guess guess = new Guess()
            .content(UPDATED_CONTENT)
            .score(UPDATED_SCORE)
            .type(UPDATED_TYPE)
            .dateCreated(UPDATED_DATE_CREATED)
            .dateModified(UPDATED_DATE_MODIFIED);
        return guess;
    }

    @BeforeEach
    public void initTest() {
        guess = createEntity(em);
    }

    @Test
    @Transactional
    public void createGuess() throws Exception {
        int databaseSizeBeforeCreate = guessRepository.findAll().size();

        // Create the Guess
        GuessDTO guessDTO = guessMapper.toDto(guess);
        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isCreated());

        // Validate the Guess in the database
        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeCreate + 1);
        Guess testGuess = guessList.get(guessList.size() - 1);
        assertThat(testGuess.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testGuess.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(testGuess.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testGuess.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testGuess.getDateModified()).isEqualTo(DEFAULT_DATE_MODIFIED);
    }

    @Test
    @Transactional
    public void createGuessWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = guessRepository.findAll().size();

        // Create the Guess with an existing ID
        guess.setId(1L);
        GuessDTO guessDTO = guessMapper.toDto(guess);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Guess in the database
        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkContentIsRequired() throws Exception {
        int databaseSizeBeforeTest = guessRepository.findAll().size();
        // set the field null
        guess.setContent(null);

        // Create the Guess, which fails.
        GuessDTO guessDTO = guessMapper.toDto(guess);

        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkScoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = guessRepository.findAll().size();
        // set the field null
        guess.setScore(null);

        // Create the Guess, which fails.
        GuessDTO guessDTO = guessMapper.toDto(guess);

        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = guessRepository.findAll().size();
        // set the field null
        guess.setType(null);

        // Create the Guess, which fails.
        GuessDTO guessDTO = guessMapper.toDto(guess);

        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateCreatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = guessRepository.findAll().size();
        // set the field null
        guess.setDateCreated(null);

        // Create the Guess, which fails.
        GuessDTO guessDTO = guessMapper.toDto(guess);

        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateModifiedIsRequired() throws Exception {
        int databaseSizeBeforeTest = guessRepository.findAll().size();
        // set the field null
        guess.setDateModified(null);

        // Create the Guess, which fails.
        GuessDTO guessDTO = guessMapper.toDto(guess);

        restGuessMockMvc.perform(post("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGuesses() throws Exception {
        // Initialize the database
        guessRepository.saveAndFlush(guess);

        // Get all the guessList
        restGuessMockMvc.perform(get("/api/guesses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(guess.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT)))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(DEFAULT_DATE_CREATED.toString())))
            .andExpect(jsonPath("$.[*].dateModified").value(hasItem(DEFAULT_DATE_MODIFIED.toString())));
    }
    
    @Test
    @Transactional
    public void getGuess() throws Exception {
        // Initialize the database
        guessRepository.saveAndFlush(guess);

        // Get the guess
        restGuessMockMvc.perform(get("/api/guesses/{id}", guess.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(guess.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.dateCreated").value(DEFAULT_DATE_CREATED.toString()))
            .andExpect(jsonPath("$.dateModified").value(DEFAULT_DATE_MODIFIED.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGuess() throws Exception {
        // Get the guess
        restGuessMockMvc.perform(get("/api/guesses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGuess() throws Exception {
        // Initialize the database
        guessRepository.saveAndFlush(guess);

        int databaseSizeBeforeUpdate = guessRepository.findAll().size();

        // Update the guess
        Guess updatedGuess = guessRepository.findById(guess.getId()).get();
        // Disconnect from session so that the updates on updatedGuess are not directly saved in db
        em.detach(updatedGuess);
        updatedGuess
            .content(UPDATED_CONTENT)
            .score(UPDATED_SCORE)
            .type(UPDATED_TYPE)
            .dateCreated(UPDATED_DATE_CREATED)
            .dateModified(UPDATED_DATE_MODIFIED);
        GuessDTO guessDTO = guessMapper.toDto(updatedGuess);

        restGuessMockMvc.perform(put("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isOk());

        // Validate the Guess in the database
        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeUpdate);
        Guess testGuess = guessList.get(guessList.size() - 1);
        assertThat(testGuess.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testGuess.getScore()).isEqualTo(UPDATED_SCORE);
        assertThat(testGuess.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testGuess.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testGuess.getDateModified()).isEqualTo(UPDATED_DATE_MODIFIED);
    }

    @Test
    @Transactional
    public void updateNonExistingGuess() throws Exception {
        int databaseSizeBeforeUpdate = guessRepository.findAll().size();

        // Create the Guess
        GuessDTO guessDTO = guessMapper.toDto(guess);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGuessMockMvc.perform(put("/api/guesses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(guessDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Guess in the database
        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGuess() throws Exception {
        // Initialize the database
        guessRepository.saveAndFlush(guess);

        int databaseSizeBeforeDelete = guessRepository.findAll().size();

        // Delete the guess
        restGuessMockMvc.perform(delete("/api/guesses/{id}", guess.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Guess> guessList = guessRepository.findAll();
        assertThat(guessList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
