package org.casualcode.guessit.backend.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RoundMapperTest {

    private RoundMapper roundMapper;

    @BeforeEach
    public void setUp() {
        roundMapper = new RoundMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(roundMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(roundMapper.fromId(null)).isNull();
    }
}
