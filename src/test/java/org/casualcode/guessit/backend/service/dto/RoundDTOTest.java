package org.casualcode.guessit.backend.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.casualcode.guessit.backend.web.rest.TestUtil;

public class RoundDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoundDTO.class);
        RoundDTO roundDTO1 = new RoundDTO();
        roundDTO1.setId(1L);
        RoundDTO roundDTO2 = new RoundDTO();
        assertThat(roundDTO1).isNotEqualTo(roundDTO2);
        roundDTO2.setId(roundDTO1.getId());
        assertThat(roundDTO1).isEqualTo(roundDTO2);
        roundDTO2.setId(2L);
        assertThat(roundDTO1).isNotEqualTo(roundDTO2);
        roundDTO1.setId(null);
        assertThat(roundDTO1).isNotEqualTo(roundDTO2);
    }
}
