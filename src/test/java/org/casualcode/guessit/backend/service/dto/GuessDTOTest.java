package org.casualcode.guessit.backend.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.casualcode.guessit.backend.web.rest.TestUtil;

public class GuessDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GuessDTO.class);
        GuessDTO guessDTO1 = new GuessDTO();
        guessDTO1.setId(1L);
        GuessDTO guessDTO2 = new GuessDTO();
        assertThat(guessDTO1).isNotEqualTo(guessDTO2);
        guessDTO2.setId(guessDTO1.getId());
        assertThat(guessDTO1).isEqualTo(guessDTO2);
        guessDTO2.setId(2L);
        assertThat(guessDTO1).isNotEqualTo(guessDTO2);
        guessDTO1.setId(null);
        assertThat(guessDTO1).isNotEqualTo(guessDTO2);
    }
}
