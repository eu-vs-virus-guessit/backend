package org.casualcode.guessit.backend.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class GuessMapperTest {

    private GuessMapper guessMapper;

    @BeforeEach
    public void setUp() {
        guessMapper = new GuessMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(guessMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(guessMapper.fromId(null)).isNull();
    }
}
