import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BackendTestModule } from '../../../test.module';
import { GuessDetailComponent } from 'app/entities/guess/guess-detail.component';
import { Guess } from 'app/shared/model/guess.model';

describe('Component Tests', () => {
  describe('Guess Management Detail Component', () => {
    let comp: GuessDetailComponent;
    let fixture: ComponentFixture<GuessDetailComponent>;
    const route = ({ data: of({ guess: new Guess(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BackendTestModule],
        declarations: [GuessDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(GuessDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(GuessDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load guess on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.guess).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
