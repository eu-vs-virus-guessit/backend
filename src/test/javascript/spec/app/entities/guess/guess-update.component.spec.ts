import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { BackendTestModule } from '../../../test.module';
import { GuessUpdateComponent } from 'app/entities/guess/guess-update.component';
import { GuessService } from 'app/entities/guess/guess.service';
import { Guess } from 'app/shared/model/guess.model';

describe('Component Tests', () => {
  describe('Guess Management Update Component', () => {
    let comp: GuessUpdateComponent;
    let fixture: ComponentFixture<GuessUpdateComponent>;
    let service: GuessService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BackendTestModule],
        declarations: [GuessUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(GuessUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(GuessUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GuessService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Guess(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Guess();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
